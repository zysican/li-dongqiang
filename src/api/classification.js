import request from "@/utils/request";

export function getClassifications(params) {
  return request({
    url: "/category/page",
    method: "get",
    params,
  });
}
export function changeEmployeesStatus(data) {
  return request({
    url: "/employee",
    method: "put",
    data,
  });
}