import request from "@/utils/request";

export function getEmployees(params) {
  return request({
    url: "/employee/page",
    method: "get",
    params,
  });
}
export function changeEmployeesStatus(data) {
  return request({
    url: "/employee",
    method: "put",
    data,
  });
}

export function addEmployee(data) {
  return request({
    url: "/employee",
    method: "post",
    data,
  });
}

export function getEmployeeById(id) {
  return request({
    url: `/employee/${id}`,
    method: "get",
  });
}
